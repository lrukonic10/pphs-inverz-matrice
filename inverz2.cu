#include<stdio.h>
__global__ void inverz2(int *rezultat, int*matrica, int K0, int K1, int K2, int det, int detInv)
{
  const int idx = threadIdx.y * blockDim.x + threadIdx.x;
  if(idx == 0)
    {
      rezultat[idx] = K0 * detInv;
    }
  if(idx == 3)
    {
      rezultat[idx] = -(K1 * detInv);
    }
  if(idx == 6)
    {
      rezultat[idx] = K2 * detInv;
    }
  if(idx == 1)
    {
      rezultat[idx] = - ((matrica[idx+2]*matrica[idx+7]) - (matrica[idx+5]*matrica[idx+4]));
    }
  if(idx == 2)
    {
      rezultat[idx] = (matrica[idx+1]*matrica[idx+5]) - (matrica[idx+4]*matrica[idx+2]);
    }
  if(idx == 4)
    {
      rezultat[idx] = (matrica[idx-4]*matrica[idx+4])-(matrica[idx+2]*matrica[idx-2]);
    }
  if(idx == 5)
    {
      rezultat[idx] = -((matrica[idx-5]*matrica[idx+2])-(matrica[idx+1]*matrica[idx-4]));
    }
  if(idx == 7)
    {
      rezultat[idx] = -((matrica[idx-7]*matrica[idx-2])-(matrica[idx-4]*matrica[idx-5]));
    }
  if(idx == 8)
    {
      rezultat[idx] = (matrica[idx-8]*matrica[idx-4])-(matrica[idx-5]*matrica[idx-7]);
    }
}


