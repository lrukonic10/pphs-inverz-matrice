import numpy as np
from numpy import linalg
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import time

mod = SourceModule(open("inverz2.cu").read())

A = np.array([[2,1,3],[4,2,7],[3,1,5]], dtype = np.int32)
print 'Matrica:\n', A

print 'Inverz matrice CPU\n'
start_cpu = time.time()
I_CPU = linalg.inv(A)
det = linalg.det(A)
end_cpu = time.time()
print  I_CPU
print 'CPU vrijeme: ', (end_cpu-start_cpu)
A_t = np.transpose(A)
print 'Transponirana matrica A:\n', A_t
print 'Determinanta matrice',det

K0 = (A[1,1]*A[2,2]) - (A[1,2]*A[2,1])
K1 = (A[1,0]*A[2,2]) - (A[1,2]*A[2,0])
K2 = (A[1,0]*A[2,1]) - (A[1,1]*A[2,0])
print K0, K1, K2
detInv = 1/det;
print 'GPU Obrada:'
start_gpu = time.time()

inverzGPU = np.empty((3,3), dtype = np.int32)
inverz = mod.get_function("inverz2")
inverz(drv.Out(inverzGPU), drv.In(A), det, K0, K1, K2, detInv, block=(3,3,1), grid=(1,1))
print inverzGPU
end_gpu = time.time()
print 'GPU vrijeme: ', (end_gpu-start_gpu)


